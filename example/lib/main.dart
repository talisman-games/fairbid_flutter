import 'package:fairbid_flutter_example/banner_view_ads.dart';
import 'package:fairbid_flutter_example/full_screen_ads.dart';
import 'package:fairbid_flutter_example/banner_ads.dart';
import 'package:fairbid_flutter_example/gdpr_controls.dart';
import 'package:fairbid_flutter_example/user_data_form.dart';
import 'package:flutter/material.dart';
import 'dart:async';

import 'package:flutter/services.dart';
import 'package:fairbid_flutter/fairbid.dart';

void main() => runApp(MyApp());

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  String _fairbidSdkVersion = 'Unknown';

  FairBid _sdk;

  bool _enableLogs = true;

  TextEditingController _sdkIdController;

  String _appId;

  int _step;

  @override
  void initState() {
    super.initState();
    initPlatformState();
    _sdkIdController = TextEditingController();
    _step = 0;
  }

  @override
  void dispose() {
    _sdkIdController.dispose();
    super.dispose();
  }

  // Platform messages are asynchronous, so we initialize in an async method.
  Future<void> initPlatformState() async {
    String fairbidSdkVersion;
    // Platform messages may fail, so we use a try/catch PlatformException.
    try {
      fairbidSdkVersion = await FairBid.version;
    } on PlatformException {
      fairbidSdkVersion = 'Failed to get SDK version.';
    }

    // If the widget was removed from the tree while the asynchronous platform
    // message was in flight, we want to discard the reply rather than calling
    // setState to update our non-existent appearance.
    if (!mounted) return;

    setState(() {
      _fairbidSdkVersion = fairbidSdkVersion;
    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: const Text('FairBid example app'),
        ),
        body: Builder(
          builder: (context) => Column(
            children: <Widget>[
              Text('Running on: $_fairbidSdkVersion\n'),
              Expanded(
                child: Stepper(
                  type: StepperType.vertical,
                  currentStep: _step,
                  onStepTapped: (step) => setState(() => _step = step),
                  controlsBuilder: (context,
                          {VoidCallback onStepContinue,
                          VoidCallback onStepCancel}) =>
                      Container(),
                  steps: <Step>[
                    Step(
                      title: Text("Setup SDK"),
                      isActive: _sdk == null,
                      subtitle: _appId == null ? null : Text("App ID: $_appId"),
                      content: _buildFirstStep(),
                    ),
                    Step(
                      title: Text("User data"),
                      isActive: _sdk != null,
                      subtitle: Text("Optional"),
                      content: _sdk != null ? UserDataForm() : Container(),
                    ),
                    Step(
                      isActive: _sdk != null,
                      title: Text("Ads control"),
                      content: _sdk != null
                          ? _buildSdkWidgets(context)
                          : Container(),
                    ),
                    Step(
                      isActive: _sdk != null,
                      title: Text("Banners control"),
                      content:
                          _sdk != null ? BannerAds(sdk: _sdk) : Container(),
                    ),
                    Step(
                      isActive: _sdk != null,
                      title: Text("Banner views control"),
                      subtitle: Text(
                        "Experimental",
                        style: TextStyle(color: Colors.deepOrangeAccent),
                      ),
                      content: _sdk != null ? BannerViewAds() : Container(),
                    ),
                    Step(
                      isActive: _sdk != null,
                      title: Text("Test suite"),
                      content: OutlineButton(
                        child: Text("Open Test suite"),
                        onPressed: () => _sdk?.showTestSuite(),
                      ),
                    ),
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildFirstStep() => Column(
        children: <Widget>[
          Row(
            children: <Widget>[
              Expanded(
                child: Text("Enable debug logs"),
              ),
              Switch.adaptive(
                value: _enableLogs,
                onChanged: (enable) => setState(() => _enableLogs = enable),
              )
            ],
          ),
          Row(
            children: <Widget>[
              Expanded(
                child: TextField(
                  keyboardType: TextInputType.numberWithOptions(),
                  controller: _sdkIdController,
                  decoration: InputDecoration(hintText: "Publisher Id"),
                ),
              ),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 6.0),
                child: OutlineButton(
                  onPressed: () {
                    _initSDK();
                  },
                  child: Text("Start SDK"),
                ),
              ),
            ],
          ),
          GDPRControls(),
        ],
      );

  Widget _buildSdkWidgets(BuildContext context) {
    return FutureBuilder<bool>(
      future: _sdk.started,
      builder: (context, snapshot) => snapshot.hasData && snapshot.data
          ? FullScreenAds(sdk: _sdk)
          : Container(
              child: Center(
                child: CircularProgressIndicator(),
              ),
            ),
    );
  }

  Future<void> _initSDK() async {
    var sdk = FairBid.forOptions(Options(
      appId: _sdkIdController.text.trim(),
      debugLogging: _enableLogs,
    ));
    await sdk.started;
    setState(() {
      _sdk = sdk;
      _appId = _sdkIdController.text;
      // go to full screen ads
      _step = 2;
    });
  }
}
