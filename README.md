# fairbid_flutter

Flutter plugin for FairBid 2.x SDK from Fyber

Provided to [andre@talisman.games](mailto:andre@talisman.games) by mike.mueller@fyber.com for use as-is in the Gaming News mobile app. Written by lukasz.huculak@fyber.com. All edits will remain private.

## Getting Started

Before you start you need to be at least familiar with [FairBid SDK official documentation](https://developer.fyber.com/fairbid2/). Topics you should be familiar with:

- [Android]() and [iOS]() SDK setup steps - to ensure your app builds and runs with the necessary support libraries and permissions
- [Publisher UI](https://ui.fyber.com/docs) - to configure apps and prepare ad placements
- [Mediation networks integration](https://fyber-mediation.fyber.com/docs) - to learn how to setup your Android/iOS projects to provide additional mediation platforms
- Ad types provided by FairBid SDK - to know what suits your needs and how to use different ad types

## SDK setup

This plugin only handles Step 1 of the [Android](https://dev-android.fyber.com/docs) and [iOS](https://dev-ios.fyber.com/docs) SDK integration instructions.

This plugin currently includes version `2.3.0` of the Flutter FairBid SDK.

> **You must follow all additional steps (Steps 2 and beyond) for your app, as well as all steps necessary for [mediation](https://fyber-mediation.fyber.com/docs) with your intended partners.**

## Flutter Implementation

### Account setup and App ID

Create account [Publishers UI](https://console.fyber.com/sign-up) and create configurations for Android and/or iOS app. App Ids have to be used to initialize SDK as described on official documentation for [Android](https://dev-android.fyber.com/docs/initialize-the-sdk) and [iOS](https://dev-ios.fyber.com/docs/initialize-the-sdk). You need to pass App Id for the platform your app is running on.

```dart
var appId = Platform.isAndroid ? _ANDROID_APP_ID : _IOS_APP_ID;
sdk = FairBid.forOptions(
  Options(
    appId: appId
  ),
);
```

You should keep reference to the FairBid instance (`sdk` above) to create ad placement instances.

### Creating an Ad

Creating an ad follows the same process for all placement types:

1. Create an "ad holder" of a placement ID for the appropriate ad type
1. Send a request
1. Wait for a response (fill/no fill/error)
1. Show the ad if available

An "ad holder" is either a wrapper for a Flutter Widget (`BannerView`) or an object used to interact with the SDK (`BannerAd`, `InterstitialAd`, `RewardedAd`).

#### Banner Placements

You can create a banner ad in two different ways:

1. As a Widget you can embed anywhere in your app's widget tree, or
1. As a floating banner at the top or bottom of the screen

> `BannerView` Widgets automatically try to load and refresh the ad placement. You don't need to manually send a request, wait for the response, or show the ad once available when using this Widget.

To create a banner ad as a Widget you can embed in your widget tree:

```dart
@override
Widget build(BuildContext context) {
  return BannerAd(placement: 'fyber placement id');
}
```

It is recommended to reserve a fixed size for the banner, otherwise the Widget will change size as the ad is loaded or refreshed:

```dart
@override
Widget build(BuildContext context) {
  return Container(
    width: 320.0,
    height: 50.0, // Banner: 50 or 90, MREC: 250
    child: BannerAd(placement: '<fyber placement id>'),
  );
}
```

To create a floating banner placement:

```dart
  var bannerAd = sdk.prepareBanner(placementId);
```

#### Interstitial Placements

To create an interstitial ad:

```dart
  var interstitialAd = sdk.prepareInterstitial(placementId);
```

#### Rewarded Placements

To create a rewarded ad:

```dart
var rewardedAd = sdk.prepareRewarded(placementId);
```

### Request, Fill, Show

Send a request:

```dart
await ad.request();
```

> Please note that the completion of `request()` doesn't mean the ad is available, only that requesting process has been started.

Wait for a response:

```dart
var adAvailable = await ad.isAvailable();
```

Show the ad if available:

```dart
if (adAvailable) {
  await ad.show();
  // or
  await bannerAd.show(alignment: BannerAlignment.bottom);
}
```
