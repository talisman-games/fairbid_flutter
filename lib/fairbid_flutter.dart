import 'dart:async';

import 'package:flutter/services.dart';

class FairbidFlutter {
  static const MethodChannel _channel =
      const MethodChannel('fairbid_flutter');

  static Future<String> get platformVersion async {
    final String version = await _channel.invokeMethod('getPlatformVersion');
    return version;
  }
}
