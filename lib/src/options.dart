part of 'fairbid_flutter.dart';

class Options {
  final String appId;
  final bool autoRequesting;
  final bool debugLogging;

  Options({
    @required this.appId,
    this.autoRequesting = true,
    this.debugLogging = false,
  })  : assert(appId != null && appId.isNotEmpty);

  Map<String, dynamic> _toMap() => {
        "publisherId": appId,
        "autoRequesting": autoRequesting,
        "logging": debugLogging,
      };
}
