part of 'fairbid_flutter.dart';

class UserData {
  static Future<UserData> getCurrent() async {
    if (_instance != null) {
      return _instance;
    } else {
      Map<String, dynamic> currentData =
          await FairBidInternal._channel.invokeMapMethod("getUserData");
      _instance = UserData._fromMap(currentData);
      return _instance;
    }
  }

  final Map<String, dynamic> _userData;

  Gender get gender {
    switch (_userData['gender']) {
      case "m":
        return Gender.male;
      case "f":
        return Gender.female;
      case "o":
        return Gender.other;
      default:
        return Gender.unknown;
    }
  }

  set gender(Gender gender) {
    var code = "u";
    switch (gender) {
      case Gender.other:
        code = "o";
        break;
      case Gender.male:
        code = "m";
        break;
      case Gender.female:
        code = "f";
        break;
      default:
        code = "u";
    }
    _userData["gender"] = code;
    _updateInstance(_userData);
  }

  DateTime get birthday {
    if (_userData.containsKey('birthday')) {
      var birthdayMap = _userData['birthday'] as Map<String, int>;
      return DateTime(
          birthdayMap['year'], birthdayMap['month'], birthdayMap['day']);
    }
    return null;
  }

  set birthday(DateTime date) {
    if (date != null) {
      var birthdayMap = <String, int>{
        'year': date.year,
        'month': date.month,
        'day': date.day,
      };
      _userData['birthday'] = birthdayMap;
    } else {
      _userData.remove('birthday');
    }
    _updateInstance(_userData);
  }

  Location get location {
    if (_userData.containsKey('location')) {
      var locationData = _userData['location'] as Map<String, double>;
      return Location(locationData['latitude'], locationData['longitude']);
    }
    return null;
  }

  set location(Location location) {
    if (location != null) {
      var locationData = <String, double>{
        'latitude': location.latitude,
        'longitude': location.longitude,
      };
      _userData['location'] = locationData;
    } else {
      _userData.remove('location');
    }
    _updateInstance(_userData);
  }

  String get id => _userData['id'];

  set id(String id) {
    _userData['id'] = id;
    _updateInstance(_userData);
  }

  @override
  String toString() {
    return 'UserData{ $_userData }';
  }

  static UserData _instance;
  UserData._fromMap(this._userData) : assert(_userData != null);

  static void _updateInstance(Map<String, dynamic> userData) async {
    await FairBidInternal._channel.invokeMethod("updateUserData", userData);
    Map<String, dynamic> currentData =
        await FairBidInternal._channel.invokeMapMethod("getUserData");
    _instance = UserData._fromMap(currentData);
  }
}

enum Gender {
  unknown,
  male,
  female,
  other,
}

class Location {
  final double latitude;
  final double longitude;

  Location(this.latitude, this.longitude);

  @override
  String toString() => "[$latitude, $longitude]";
}
