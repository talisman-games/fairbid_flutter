import 'dart:io';

import 'package:fairbid_flutter/src/fairbid_flutter.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:meta/meta.dart';

/// shared channel
const MethodChannel _channel = FairBidInternal.methodCallChannel;

@experimental
class BannerView extends StatefulWidget {
  BannerView({Key key,
    this.placement,
    this.placeholderColor = const Color(0xFFF2F2F2)})
      : super(key: key);

  final String placement;
  final Color placeholderColor;

  @override
  _BannerViewState createState() => _BannerViewState();
}

class _BannerViewState extends State<BannerView> {
  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(builder: (context, constraints) {
      // we need to tell native code what size of banners it can fit into the view
      final viewConstraints = <String, int>{};
      if (constraints.hasBoundedHeight) {
        viewConstraints["height"] = constraints.biggest.height.floor();
      }
      if (constraints.hasBoundedWidth) {
        viewConstraints["width"] = constraints.biggest.width.floor();
      }
      final orientation = MediaQuery
          .of(context)
          .orientation;
      return _NativeBannerWrapper(
        orientation: orientation,
        placement: widget.placement,
        placeholderColor: widget.placeholderColor,
        viewConstraints: viewConstraints,
      );
    });
  }
}

class _NativeBannerWrapper extends StatefulWidget {
  const _NativeBannerWrapper({Key key,
    this.placement,
    this.viewConstraints,
    this.orientation,
    this.placeholderColor})
      : super(key: key);

  final String placement;
  final Map<String, int> viewConstraints;
  final Orientation orientation;
  final Color placeholderColor;

  @override
  _FBBannerState createState() => _FBBannerState();
}

class _FBBannerState extends State<_NativeBannerWrapper> {
  static final _messageCodec = const StandardMessageCodec();
  static final _viewType = "bannerView";
  Size _size;
  Widget nativeBanner;

  Map<String, dynamic> get bannerParams =>
      Map<String, dynamic>.from(widget.viewConstraints)
        ..putIfAbsent("placement", () => widget.placement);

  @override
  void initState() {
    super.initState();
    _loadBanner();
  }

  void _loadBanner() async {
    try {
      final dimensionsArray = await _channel.invokeMethod<List<dynamic>>(
        "loadBanner",
        bannerParams,
      );
      double width =
      dimensionsArray[0] <= 0 ? double.infinity : dimensionsArray[0];
      double height = dimensionsArray[1];
      setState(() {
        _size = Size(width, height);
      });
      print(dimensionsArray);
    } catch (e) {
      _size = null;
      print(e);
    }
  }

  @override
  void dispose() {
    print('banner view disposed');
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    Widget adPlaceholder() {
      return Container(
        color: widget.placeholderColor,
        child: const Center(
            child: Text(
              'AD',
              style: TextStyle(color: Color(0xFFBDBDBD), fontSize: 14),
            )),
      );
    }

    Widget nativeView;
    Widget ad() {
      PlatformViewCreatedCallback callback =
          (id) => print("Banner view created: $id");
      if (Platform.isAndroid) {
        nativeView = AndroidView(
          onPlatformViewCreated: callback,
          viewType: _viewType,
          creationParams: bannerParams,
          creationParamsCodec: _messageCodec,
        );
      } else {
        nativeView = UiKitView(
          viewType: _viewType,
          creationParams: bannerParams,
          creationParamsCodec: _messageCodec,
          onPlatformViewCreated: callback,
        );
      }
      nativeBanner = Center(
        child: SizedBox(
          width: _size.width,
          height: _size.height,
          child: nativeView,
        ),
      );
      return nativeBanner;
    }

    return _size != null ? ad() : adPlaceholder();
  }
}
