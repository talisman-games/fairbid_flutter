part of 'fairbid_flutter.dart';

/// Allows for passing user GDPR consent information.
class GDPR {
  GDPR._();
  /// Update GDPR consent data
  ///
  static Future<void> updateConsent(
      {@required bool grantsConsent, Map<String, String> consentData}) {
    assert(grantsConsent != null);
    final params = <String, Object>{
      "grantConsent": grantsConsent,
      if (consentData != null) "consentData": consentData,
    };
    return FairBidInternal._channel.invokeMethod("updateGDPR", params);
  }

  /// Clears all GDPR related data
  static Future<void> clearConsent() =>
      FairBidInternal._channel.invokeMethod("clearGDPR");
}
